"""
All city data data collection and scoring is handled here.
"""
import pandas as pd

from app import paths
from app import weather
from app import patterns


class Cities(metaclass=patterns.Singleton):
    """
    All city data data collection and scoring is handled here.
    """
    def __init__(self):
        self.csv_file = paths.CITIES_CSV
        self.data = None
        self._city_score()

    def city_data(self, city):
        """
        Collect and score a single city.
        :param city: Name of the city.
        :return: Dictionary of city data or None if the city data cannot be found.
        """
        city = city.lower()
        if city in self.data.index:
            city_weather = weather.MetaWeather(city)
            score = self._combined_score(city, city_weather)
            details = self.data.drop(['score'], axis=1).loc[city].to_dict()
            details = self._convert(details)
            details['city_score'] = score
            details["city_name"] = city
            details["current_temperature"] = city_weather.current_temp
            details["current_weather_description"] = city_weather.current_description()
            return details
        return None

    def ranking(self, cities):
        """
        Presents data for each city, scores the city and then ranks them relative to each other.
        :param cities: list of city names.
        :return: List of city details.
        """
        known = []
        unknown = []
        for city in cities:
            city_details = self.city_data(city)
            if city_details:
                city_details["state"] = "present"
                known.append(city_details)
            else:
                unknown.append({"city_name": city,
                                "state": "unknown"})

        sorted_known = sorted(known, key=lambda city: city["city_score"], reverse=True)
        for rank, city_details in enumerate(sorted_known, start=1):
            city_details["city_rank"] = rank

        return sorted_known + unknown  # Don't rank a city if we have no info on it.

    def reload_state(self):
        """
        Reload the city data and score if there are changes to the csv.
        """
        self._city_score()

    def _convert(self, details):
        """
        Needed as pandas still has a bug which does not convert the types yet.  Released in new pre build.
        """
        for key, value in details.items():
            if type(value) == pd.np.int64:
                details[key] = int(value)
            elif type(value) == pd.np.float64:
                details[key] = float(value)
        return details

    def _combined_score(self, city,  weather):
        """
        Combine city score and weather score.
        """
        total_score = 0
        total_score = self.data.loc[city]["score"]
        if weather:
            total_score += weather.score()
        total_score /= 2   # If there is no weather score then incomplete and so halved
        return total_score

    def _city_score(self):
        """
        Just scoring a city using the crime rate and public transport.
        """
        self._load()
        self.data.index = self.data.index.str.lower()
        self.data['crime_rate_inverse'] = 10 - self.data['crime_rate']
        self.data["score"] = self.data[['public_transport', 'crime_rate_inverse']].mean(axis=1)

        # not needed.
        del self.data['index']
        del self.data["crime_rate_inverse"]

    def _load(self):
        """
        Read the CSV.
        """
        self.data = pd.read_csv(self.csv_file, index_col="city")
