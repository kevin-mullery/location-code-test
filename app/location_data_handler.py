from tornado.web import RequestHandler
from http import HTTPStatus
import json

from app import destination


class LocationHandler(RequestHandler):

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.cities = destination.Cities()

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, city):
        """
        Retrieve information about a city.
        :param city:  The name of the city.
        :param kwargs:
        :return:
        """
        response = self.cities.city_data(city)
        if response:
            self.set_status(HTTPStatus.OK)
            self.write(json.dumps(response))
        else:
            self.set_status(HTTPStatus.BAD_REQUEST)
