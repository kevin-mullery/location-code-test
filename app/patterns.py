"""
General Development patterns.
"""


class Singleton(type):
    """
    Standard metaclass singleton pattern.
    Can be used for multiple class types.
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]
