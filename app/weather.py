"""
Weather details.
"""
import json

import requests
from requests import compat


class MetaWeather:
    """
    Using the metaweather public API.
    """
    _BASE_URL = "https://www.metaweather.com/api/"
    _LOCATION = "location/search/"
    _WEATHER = "location/"

    def __init__(self, city):
        self.city = city.lower()
        self._forecast = None
        self._error_msg = ""
        self._retrieve()

    @property
    def current_temp(self):
        """
        The current temperature. Metweather doesn't include the temperature unit, but it is celsius.
        """
        if self._forecast:
            return self._forecast['consolidated_weather'][0]['the_temp']
        return None

    def score(self):
        """
        Score is based on the weather state. Score ranges from 1 - 10 with 10 being good.
        """
        scores = {"sn": 1, "sl": 2, "h": 3, "t": 4, "hr": 5, "lr": 6, "s": 7, "hc": 8, "lc": 9, "c": 10}
        if self._forecast:
            weather_score = 0
            for daily_forecast in self._forecast['consolidated_weather']:
                weather_score += scores.get(daily_forecast["weather_state_abbr"], 0)
            return weather_score / len(self._forecast['consolidated_weather'])  # Average score.
        return None

    def current_description(self):
        if self._forecast:
            state = self._forecast['consolidated_weather'][0]["weather_state_name"]
            min_temp = self._forecast['consolidated_weather'][0]["min_temp"]
            max_temp = self._forecast['consolidated_weather'][0]["max_temp"]
            return f"{state} with temperatures ranging from {min_temp} to {max_temp} degrees celsius."
        return None

    @property
    def error_message(self):
        return self._error_msg

    def _retrieve(self):
        try:
            woeid = self._woeid()
            if woeid:
                url = compat.urljoin(self._BASE_URL, self._WEATHER, woeid)
                weather_url = f"{url}{woeid}/"
                response = requests.get(weather_url)
                if response.status_code == 200:
                    self._forecast = json.loads(response.text)
        except ConnectionError:
            self._error_msg = "Unable to retrieve weather forecast."

    def _woeid(self):
        location_url = compat.urljoin(self._BASE_URL, self._LOCATION)
        response = requests.get(location_url, {'query': self.city})
        if response.status_code == 200:
            city = json.loads(response.text)
            if len(city) == 1:
                return city[0]["woeid"]
            self._error_msg = f"City not specific enough: '{self.city}'?"
        return None

    def __bool__(self):
        if self._forecast:
            return True
        return False
