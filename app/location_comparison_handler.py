from tornado.web import RequestHandler
from http import HTTPStatus
import json

from app import destination


class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.cities = destination.Cities()

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, cities):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param cities: List of cities.
        """
        city_list = cities.split(",")
        city_response = self.cities.ranking(city_list)

        response = {'city_data': city_response}

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
