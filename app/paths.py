"""
Keeps track of all of the paths in the application.
"""
import os
PROJECT_ROOT = os.path.split(os.path.dirname(os.path.abspath(__file__)))[0]
DATA_DIR = os.path.join(PROJECT_ROOT, "data")
CITIES_CSV = os.path.join(DATA_DIR, "european_cities.csv")
